
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// PRAugmentedReality
#define COCOAPODS_POD_AVAILABLE_PRAugmentedReality
#define COCOAPODS_VERSION_MAJOR_PRAugmentedReality 2
#define COCOAPODS_VERSION_MINOR_PRAugmentedReality 1
#define COCOAPODS_VERSION_PATCH_PRAugmentedReality 0

// PRAugmentedReality/AR
#define COCOAPODS_POD_AVAILABLE_PRAugmentedReality_AR
#define COCOAPODS_VERSION_MAJOR_PRAugmentedReality_AR 2
#define COCOAPODS_VERSION_MINOR_PRAugmentedReality_AR 1
#define COCOAPODS_VERSION_PATCH_PRAugmentedReality_AR 0

// PRAugmentedReality/Location
#define COCOAPODS_POD_AVAILABLE_PRAugmentedReality_Location
#define COCOAPODS_VERSION_MAJOR_PRAugmentedReality_Location 2
#define COCOAPODS_VERSION_MINOR_PRAugmentedReality_Location 1
#define COCOAPODS_VERSION_PATCH_PRAugmentedReality_Location 0

