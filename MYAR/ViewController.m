//
//  ViewController.m
//  MYAR
//
//  Created by Trash on 6/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController ()<CLLocationManagerDelegate,ARControllerDelegate>{
    CLLocationManager *manager;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    manager = [CLLocationManager new];
    manager.delegate = self;
    [manager startUpdatingLocation];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showAR:(id)sender {
    ARObject *SGMansion = [[ARObject alloc] initWithId:1 title:@"SGMansion" coordinates:CLLocationCoordinate2DMake(13.775228f, 100.563775) andCurrentLocation:manager.location.coordinate];
    ARController *arController = [[ARController alloc] initWithScreenSize:self.view.frame.size andDelegate:self];
    
    [arController startARWithData:@[[SGMansion getARObjectData]] andCurrentLoc:manager.location.coordinate];
}

- (void)arControllerDidSetupAR:(UIView *)arView withCameraLayer:(AVCaptureVideoPreviewLayer*)cameraLayer {
    [self.view.layer addSublayer:cameraLayer];
    [self.view addSubview:arView];
}
- (void)arControllerUpdateFrame:(CGRect)arViewFrame {
    [[self.view viewWithTag:AR_VIEW_TAG] setFrame:arViewFrame];
}
- (void)gotProblemIn:(NSString*)problemOrigin withDetails:(NSString*)details {
    NSLog(@"Error in %@: %@", problemOrigin, details);
}
@end
